 <?php   
if (!defined('_PS_VERSION_'))
  exit;
class md1 extends Module
{
	public function __construct()
  	{
	    $this->name = 'md1';
	    $this->tab = 'front_office_features';
	    $this->version = '1.0.0';
	    $this->author = 'Xung';
	    $this->need_instance = 0;
	    $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_); 
	    $this->bootstrap = true;
	 
	    parent::__construct();
	 
	    $this->displayName = $this->l('My module');
	    $this->description = $this->l('Mô tả module.');
  	}


  	function install()
	{
		if (!parent::install())
		return false;
		if (!$this->registerHook('leftColumn'))
		return false;
		return true;
	}
// Hàm gỡ cài đặt
	public function uninstall()
 	{
 		if (!parent::uninstall()) return false;
 		if( !Configuration::deleteByName('example_content')) return false;
 		return true;
 	}
//Hiển thị nội dung
 /**
 * Returns module content
 *
 * @param array $params Parameters
 * @return string Content
 */
// Gắn vào 1 hook: cột trái LeftColumn hoặc cột phải RightColumn
 	function hookLeftColumn($params)
 	{
	global $smarty;
	//$smarty->output_charset = 'HTML';
	$examplecontent=Configuration::get('example_content');
	//$examplecontent=html_entity_decode($examplecontent);
	$smarty->assign('content',$examplecontent);
	return $this->display(__FILE__, 'example.tpl');
 	}

	function hookRightColumn($params)
 	{
 		return $this->hookLeftColumn($params);
 	}


// Lấy nội dung tại Back Office
	public function getContent()
 	{
 		$output = '<h2>'.$this->displayName.'</h2>';
 		if (Tools::isSubmit('submit') )
 	{

 		Configuration::updateValue('example_content', $gai, true);
 		$output .= '
 		<div class="confirm">     
 		'.$this->l('Settings updated').'
 		</div>';
 	}
 	return $output.$this->displayForm();
 	}
// Hiển thị form nhập nội dung tại Back Office
 	public function displayForm()
 	{
 		$output = '
 		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
 		<fieldset><legend>'.$this->l('Settings').'</legend>
 		<label>'.$this->l('Your content').'</label>
 		<div class="margin-form">
 		<textarea name="example_content" cols="90" rows="10" />'.Tools::getValue('example_content', Configuration::get('		example_content')).'</textarea>
 		</div>
 		<input type="submit" name="submit" value="'.$this->l('Update settings').'" class="button" />
 		</fieldset>
 		</form>';
 		return $output;
 	}
}
